import axios from 'axios';


const EMPLOYEE_API_BASE_URL = "http://localhost:3000";

class EmployeeService {

    getEmployees(){
        return axios.get(EMPLOYEE_API_BASE_URL + "/employeesList");
    }

    deleteEmployeeBySss(SSSNumber){
        return axios.delete(EMPLOYEE_API_BASE_URL + "/employee/delete/sss/" + SSSNumber);
    }


    deleteEmployeeInactive(status) {
        return axios.delete(EMPLOYEE_API_BASE_URL + "/employee/delete/status/" + status)
    }

    searchEmployee(sssNumber, firstName){
        return axios.get(EMPLOYEE_API_BASE_URL+'/emp/' + sssNumber + '/' +firstName)
    }
    getById(id){
        return axios.get(EMPLOYEE_API_BASE_URL + `/employeesList/employee/` + id)
    }
}


export default new EmployeeService()    