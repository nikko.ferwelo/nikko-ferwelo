import React, { Component } from 'react';
import '../App.css';
import AppNavbar from '../AppNavbar';
import EmployeeService from '../service/EmployeeService';
class Search_Employee extends React.Component {
    constructor(props) {
        super(props)
       
        this.state = {
                employees: [],
                sssNumber: "",
                firstName: "",
                isLoading: true,
        }
        this.searchEmployee = this.searchEmployee.bind(this);
        this.handleChangeSSS = this.handleChangeSSS.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
    }
  
    
    handleChangeSSS = (e) =>{ 
        this.setState({sssNumber: e.target.value});
      }
    handleChangeName = (e) =>{ 
        this.setState({firstName: e.target.value});
      }
   
    searchEmployee(sssNumber, firstName) {
      EmployeeService.searchEmployee(sssNumber, firstName).then( res => {
        this.setState({employees: res.data});

    });
    }

    
    render() {
       
    
        return (
            
          <div>
            <AppNavbar/>
            <form>
                
                <label className="form-control">
                SSS Number:
                <input type="number" className="form-control" value={this.sssNumber} onChange={this.handleChangeSSS}/>
                </label>
                <label className="form-control">
                First Name:
                <input type="text" className="form-control" value={this.firstName} onChange={this.handleChangeName}/>

                </label>
                <button className="btn btn-info" onClick={this.searchEmployee(this.state.sssNumber, this.state.firstName)}>Search</button>

                
            </form>
            
          </div>
        );
      }
    }

export default Search_Employee;