package com.bootcamp.hris_group1w3.resource.impl;


import com.bootcamp.hris_group1w3.employees.Employees;
import com.bootcamp.hris_group1w3.service.IPageService;
import com.bootcamp.hris_group1w3.resource.Resource;
import com.bootcamp.hris_group1w3.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/employees")
@CrossOrigin(origins="http://localhost:3000")
public class SearchResouceImpl implements Resource<Employees> {
    @Autowired
    private IService<Employees> employeeService;

    @Autowired
    private IPageService<Employees> employeesIPageService;

    @Override
    public ResponseEntity<Page<Employees>> findAll(Pageable pageable, String searchText) {
        return new ResponseEntity<>(employeesIPageService.findAll(pageable, searchText), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Page<Employees>> findAll(int pageNumber, int pageSize, String sortBy, String sortDir) {
        return new ResponseEntity<>(employeesIPageService.findAll(PageRequest.of(pageNumber, pageSize,sortDir.equalsIgnoreCase("asc") ? Sort.by(sortBy).ascending() : Sort.by(sortBy).descending())), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Employees> findById(Long id) {
        return new ResponseEntity<>(employeeService.findById(id).get(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Employees> save(Employees employees) {
        return new ResponseEntity<>(employeeService.saveOrUpdate(employees), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Employees> update(Employees employees) {
        return new ResponseEntity<>(employeeService.saveOrUpdate(employees), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> deleteById(Long id) {
        return new ResponseEntity<>(employeeService.deleteById(id), HttpStatus.OK);
    }
}
