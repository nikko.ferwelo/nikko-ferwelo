package com.bootcamp.hris_group1w3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class HrisGroup1W3Application {



    public static void main(String[] args) {
        SpringApplication.run(HrisGroup1W3Application.class, args);
    }



}
