package com.bootcamp.hris_group1w3.employees;

import lombok.Data;

@Data
public class EmployeeResponseBody {
        private String message;
        private Object payload;

        public EmployeeResponseBody(String message, Object payload) {
            this.message = message;
            this.payload = payload;
        }
}
