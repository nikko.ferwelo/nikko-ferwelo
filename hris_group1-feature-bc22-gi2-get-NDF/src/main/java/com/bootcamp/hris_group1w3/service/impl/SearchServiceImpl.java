package com.bootcamp.hris_group1w3.service.impl;
import java.util.Collection;
import java.util.Optional;
import com.bootcamp.hris_group1w3.employees.Employees;
import com.bootcamp.hris_group1w3.repository.SearchEmployeeRepository;
import com.bootcamp.hris_group1w3.service.IService;
import com.bootcamp.hris_group1w3.service.IPageService;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class SearchServiceImpl implements IService<Employees>, IPageService<Employees>  {
    @Autowired
    private SearchEmployeeRepository searchEmployeeRepository;

    @Override
    public Collection<Employees> findAll() {
        return (Collection<Employees>) searchEmployeeRepository.findAll();
    }

    @Override
    public Page<Employees> findAll(Pageable pageable, String searchText) {
        return searchEmployeeRepository.findAllEmployees(pageable, searchText);
    }
    @Override
    public Page<Employees> findAll(Pageable pageable) {
        return searchEmployeeRepository.findAll(pageable);
    }

    @Override
    public Optional<Employees> findById(Long id) {
        return searchEmployeeRepository.findById(id);
    }

    @Override
    public Employees saveOrUpdate(Employees employees) {
        return searchEmployeeRepository.save(employees);
    }

    @Override
    public String deleteById(Long id) {
        JSONObject jsonObject = new JSONObject();
        try {
            searchEmployeeRepository.deleteById(id);
            jsonObject.put("message", "Employee deleted successfully");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }







}
