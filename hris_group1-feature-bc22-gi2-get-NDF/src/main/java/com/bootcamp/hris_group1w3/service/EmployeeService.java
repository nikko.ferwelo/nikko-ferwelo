package com.bootcamp.hris_group1w3.service;


import com.bootcamp.hris_group1w3.employees.EmployeeReqBody;
import com.bootcamp.hris_group1w3.employees.Employees;
import com.bootcamp.hris_group1w3.helper.UserNotFoundException;
import com.bootcamp.hris_group1w3.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;


@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository repo;

    public List<Employees> listAll(){
        return repo.findAll();
    }

    public List<Employees> getByStatus(String status) {
        if(status!=null){
            return repo.selectByStatus(status);
        }
        return repo.findAll();
    }

    public Employees save(EmployeeReqBody emp) {
        long millis=System.currentTimeMillis();

        Employees employees = new Employees();
        employees.setCreatedBy(emp.getCreatedBy());
        employees.setCreatedDate(new Date(millis));
        employees.setFirstName(emp.getFirstName());
        employees.setLastName(emp.getLastName());
        employees.setMiddleName(emp.getMiddleName());
        employees.setSSSNumber(emp.getSSSNumber());
        employees.setStatus(emp.getStatus());
        employees.setUpdatedBy(emp.getUpdatedBy());
        employees.setUpdatedDate(new Date(millis));
        return repo.save(employees);
    }

    public Employees findById(Integer id){
        final var employees = repo.findById(id).get();
        return employees;
    }

    public Employees selectBySSS(Integer sss) throws UserNotFoundException {
        Employees i = repo.selectBySSS(sss);
        if (i != null) {
            return i;
        }
        throw new UserNotFoundException("SSS number("+ sss+") did not much in any of the employees!");
    }

    public void delete(Employees e) {
        repo.delete(e);
    }

    public void deleteBatch(Iterable<Employees> emps){
        repo.deleteAllInBatch(emps);
    }

    public Iterable<Employees> selectBatch(String status) throws UserNotFoundException {
        Iterable<Employees> iemp = repo.selectByStatus(status);
        if (iemp != null) {
            return iemp;
        }
        throw new UserNotFoundException("SSS number("+ status+") did not much in any of the employees!");

    }

    public Boolean checkAttendance(Integer sss){
        if (repo.attendanceByEmployeeId(sss).isEmpty()){
            return false;
        }
        return true;
    }


    public void deleteEmployeeBySSS(Integer sssNumber) {
        Integer result = repo.deleteEmployeeBySSS(sssNumber);
        System.out.println("RESULT : ---" + result);
        if(result > 0){
            repo.deleteEmployee(sssNumber);
        }
    }
    public List<Employees> getByFnameAndSSSnumber(String firstName, Integer sssNumber) {
        if(firstName!=null){
            return repo.selectBySSSandName(firstName, sssNumber);
        }
        return repo.findAll();
    }

    public List<Employees> getSSSNumberFname(String firstName, Integer sssNumber){
        return repo.getSSSandFname(firstName,sssNumber);
    }

    public Employees updateEmployee(Employees emp) {
        Employees employee = repo.getById(emp.getId());
        employee.setUpdatedBy(emp.getUpdatedBy());
        employee.setFirstName(emp.getFirstName());
        employee.setLastName(emp.getLastName());
        employee.setMiddleName(emp.getMiddleName());
        employee.setStatus(emp.getStatus());
        employee.setSSSNumber(emp.getSSSNumber());
        employee.setUpdatedDate(new java.util.Date());

        return repo.save(employee);
    }

//    public List<Employees> getBySSSandName(String firstName) {
//        if(firstName!=null){
//            return repo.selectBySSSandName(firstName);
//        }
//        return repo.findAll();
//    }
}
