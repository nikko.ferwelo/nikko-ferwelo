package com.bootcamp.hris_group1w3.controller;

import com.bootcamp.hris_group1w3.attendance.Attendance;
import com.bootcamp.hris_group1w3.attendance.AttendanceReqBody;
import com.bootcamp.hris_group1w3.attendance.AttendanceResponse;
import com.bootcamp.hris_group1w3.service.AttendanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class AttendanceController {
    @Autowired
    private AttendanceService service;

    //Insert Attendance record
    @PostMapping("/attendance")
    public ResponseEntity<AttendanceResponse> addAttendance(@RequestBody AttendanceReqBody att){
        Attendance attendance = service.save(att);
        return new ResponseEntity<AttendanceResponse>(new AttendanceResponse("Success",attendance), HttpStatus.OK);
    }

    @GetMapping("/attendance/{id}")
    public List<Attendance> getAttendanceById(@PathVariable Integer id){
         return service.getAttendanceById(id);
    }
}
