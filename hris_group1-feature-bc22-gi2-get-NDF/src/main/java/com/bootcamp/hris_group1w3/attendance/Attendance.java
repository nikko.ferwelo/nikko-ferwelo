
package com.bootcamp.hris_group1w3.attendance;

import com.bootcamp.hris_group1w3.employees.Employees;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "employee_attendance")
@IdClass(AttendancePK.class)
public class Attendance implements Serializable {
    @Id
    @ManyToOne
    @JoinColumn(name = "id")
    @JsonIgnoreProperties(value = {"employee_attendance", "hibernateLazyInitializer"})
    private Employees employees;
    @Id
    private Date attendanceDate;
    private java.util.Date timeIn;
    private java.util.Date timeOut;
    private String shift;
    private String status;
    private String createdBy;
    private String updatedBy;
    private java.util.Date updatedDate;
}
