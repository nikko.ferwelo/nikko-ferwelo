package com.bootcamp.hris_group1w3.attendance;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class AttendanceReqBody {
    private Integer id;
    private String shift;
    private String status;
    private String createdBy;
    private String updatedBy;
}
