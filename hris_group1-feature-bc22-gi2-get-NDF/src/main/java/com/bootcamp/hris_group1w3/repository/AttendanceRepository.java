package com.bootcamp.hris_group1w3.repository;


import com.bootcamp.hris_group1w3.attendance.Attendance;
import com.bootcamp.hris_group1w3.employees.Employees;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AttendanceRepository extends JpaRepository<Attendance, Integer> {

    @Query(value = "SELECT * FROM employee_attendance ea where ea.id = :id",nativeQuery = true)
    List<Attendance> findAllAttendance(@Param("id") Integer id);

}
