package com.bootcamp.hris_group1w3.repository;

import com.bootcamp.hris_group1w3.employees.Employees;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface SearchEmployeeRepository extends PagingAndSortingRepository<Employees, Long> {

    @Query(value="SELECT * FROM employees b WHERE b.first_name LIKE %:searchText% OR b.last_name LIKE %:searchText% OR b.middle_name LIKE %:searchText% OR b.status LIKE %:searchText% ORDER BY b.id ASC",nativeQuery = true)
    Page<Employees> findAllEmployees(Pageable pageable, @Param("searchText") String searchText);
}
