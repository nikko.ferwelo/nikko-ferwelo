package com.bootcamp.hris_group1w3;

import com.bootcamp.hris_group1w3.employees.Employees;
import com.bootcamp.hris_group1w3.repository.EmployeeRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import java.util.Date;
import java.util.List;

@SpringBootTest
class HrisGroup1W3ApplicationTests {

    @Autowired
    private EmployeeRepository employeeRepository;
//    @Test
//    void contextLoads() {
//    }
//
//    @Test
//    @Order(1)
//    @DisplayName("Get by ID")
//    public void getEmployeeTest(){
//
//        Employees employee = employeeRepository.findById(1).get();
//
//        Assertions.assertThat(employee.getId()).isEqualTo(1);
//
//    }
//
//    @Test
//    @Order(2)
//    @DisplayName("Get list of Employee")
//    public void getListOfEmployeesTest(){
//
//        List<Employees> employees = employeeRepository.findAll();
//
//        Assertions.assertThat(employees.size()).isGreaterThan(0);
//
//    }
//
//
//    @Test
//    @Order(3)
//    @DisplayName("Employee Save")
//    public void updateEmployeeTest() {
//        Employees employees = employeeRepository.findById(3).get();
//        employees.setFirstName("Nikko");
//        employees.setLastName("Ratuu");
//        employees.setMiddleName("Nani");
//        employees.setUpdatedDate(new Date());
//        employees.setSSSNumber(6996);
//        employees.setUpdatedBy("Magna Carta");
//        employees.setStatus("Active");
//        employees.setCreatedBy("Nanimo");
//        employees.setCreatedDate(new Date());
//
//        Employees employeesUpdated = employeeRepository.save(employees);
//        Assertions.assertThat(employeesUpdated.getFirstName()).isEqualTo("Nikko");
//    }
//
//    @Test
//    @Order(4)
//    @DisplayName("Find by and SSSNumber")
//    public void selectBySSS(){
//        Employees employees = employeeRepository.selectBySSS(111);
//        Assertions.assertThat(employees.getSSSNumber()).isEqualTo(111);
//    }
//
//



}
