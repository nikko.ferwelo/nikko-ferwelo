import { Component, OnInit } from '@angular/core';
import { AttendanceService } from '../attendance.service';
import { Attendance } from '../attendance';
import { Employee } from '../employee';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { EmployeeService } from '../employee.service';



@Component({
  selector: 'app-add-attendance',
  templateUrl: './add-attendance.component.html',
  styleUrls: ['./add-attendance.component.css']
})
export class AddAttendanceComponent implements OnInit {

  id!: number;

  employee:Employee = new Employee();
  attendance:Attendance = new Attendance();

  constructor(private employeeService: EmployeeService,private attendanceService: AttendanceService,private route: ActivatedRoute,
     private router: Router) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.employeeService.getEmployeeById(this.id).subscribe(data => {
      this.employee = data;
      this.attendance.id = this.employee.id;
    }, error => console.log(error));
  }

  saveAttendance(){
    this.attendanceService.createAttendance(this.attendance).subscribe(data => {
      this.goToAttendanceList();
  },
  error => console.log(error)) 
  }

  goToAttendanceList(){
    this.router.navigateByUrl('/view-employee/'+`${this.id}`+'/add-attendance');
  }

  onSubmit(){
    console.log(this.attendance);
    try{
      this.saveAttendance();
    }
    catch(e){
      console.log(e);
    }
  }

}

