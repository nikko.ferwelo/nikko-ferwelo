export class Employee {
    id: number = 0;
    status: string ='';
    updatedBy: string = '';
    createdBy: string = '';
    updatedDate: String = '';
    createdDate: String = '';  
    firstName: string = '';
    lastName: string = '';
    middleName: string = '';
    sssnumber!: number;
  }