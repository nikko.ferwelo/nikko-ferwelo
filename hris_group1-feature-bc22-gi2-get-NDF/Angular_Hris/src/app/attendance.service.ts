import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Attendance } from './attendance';


@Injectable({
  providedIn: 'root'
})
export class AttendanceService {
  private baseUrl: string;

  constructor(private http: HttpClient) { 
    this.baseUrl = 'http://localhost:8080/';
  }

  createAttendance(attendance: Attendance): Observable<object> {  
    return this.http.post(`${this.baseUrl}`+'attendance', attendance);  
  }
  
  getAttendanceById(id:number): Observable<Attendance[]>{
    return this.http.get<Attendance[]>(`${this.baseUrl}`+'attendance/'+`${id}`);
  }
}





