export class Attendance {
    id!: number;
    attendanceDate!: Date;
    timeIn!: String;
    timeOut!: String;
    shift!: string;
    status!: string;
    createdBy!: string;
    updatedBy!: string;
    updatedDate!: String;
}
