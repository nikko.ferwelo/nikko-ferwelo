import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { first, Observable } from 'rxjs';
import { Employee } from './employee';
@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  private baseUrl: string;

  constructor(private http: HttpClient) {
     this.baseUrl = 'http://localhost:8080/employeesList/';
  }
  API = "http://localhost:8080";


  //Get all Employee
  public getEmployeeList(): Observable<Employee[]>  {
    return this.http.get<Employee[]>(this.baseUrl);
  }

  getData(employee: Employee ){
    let url = this.API + "/employee";
    return this.http.post(url, employee);
  }







  //Create Employee
  createEmployee(employee: Employee): Observable<object> {
    return this.http.post(`${this.baseUrl}`+'employee', employee);
  }

  //Get Employee By ID
  getEmployeeById(id:number): Observable<Employee> {
    return this.http.get<Employee>(`${this.baseUrl}`+'employee/'+`${id}`);
  }


   //DELETE By SSS no.
  public deleteEmployee(sssID: any){
    return this.http.delete(this.API + "/employee/delete/sss/"+sssID);

  }

  //Get By STATUS
  public getByStatus(status: string){
    return this.http.get(this.API + "/employees/status?="+ status);
  }

   //DELETE By Status
  public deleteEmployeeByStatus(status: string){
    return this.http.delete(this.API + "/employee/delete/status/"+status);
  }


  //UPDATE
  public updateEmployee(employee: any){
    return this.http.put(this.API+'/employee/update',employee);
  }

  public getEmployeeFilter(): Observable<Employee[]> {
    return this.http.get<Employee[]>(`${this.baseUrl}`+ '/employees');
  }

  search(sssNumber: number, name: string, ): Observable<any> {
    return this.http.get(`${this.API}/emp/${sssNumber}/${name}`);
  }

}
