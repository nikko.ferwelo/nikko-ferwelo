import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { Employee } from '../employee';
@Component({
  selector: 'app-employee-filter',
  templateUrl: './employee-filter.component.html',
  styleUrls: ['./employee-filter.component.css']
})
export class EmployeeFilterComponent implements OnInit {

  firstName!: string;
  sssNumber!: number;
  employees!: Employee[];
  constructor(private employeeService: EmployeeService) {}
  ngOnInit(){

    this.firstName = "";
    this.sssNumber;

  }

  private searchByNameSSSNum() {
    this.employeeService.search(this.sssNumber, this.firstName, )
      .subscribe(employees => this.employees = employees);
  }

  onSubmit() {
    this.searchByNameSSSNum(); {

  }

  }
}
