import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from '../employee.service';
import { Employee } from '../employee';
import Swal from 'sweetalert2';



@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {

  employee:Employee = new Employee();
  constructor(private employeeService: EmployeeService, private router:Router) { }

  ngOnInit(): void {
  }

  saveEmployee(){
    this.employeeService.createEmployee(this.employee).subscribe(data => {
        this.goToEmployeeList();
        this.alertSucess();
    },
    error => console.log(error)) 
  }

  goToEmployeeList(){
    this.router.navigate(['/employees'])
  }

  onSubmit(){
    console.log(this.employee);
    try{
      this.saveEmployee();
      
    }
    catch(e){
      console.log(e);
    }
  }

  alertSucess(){
    Swal.fire(
      'Success!', 
      'New Employee has been successfully added!',
      'success',
    );
  }

}
